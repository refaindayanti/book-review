const express = require("express");
const app = express();
const user = require("./routes/User");
const books = require("./routes/Books");
const reviews = require("./routes/Reviews");
const readinglist = require("./routes/ReadingList");
const favorite = require("./routes/Favorite");
const config = require("config");
// console.log('' + config.get("jwtPrivateKey"));

app.use(express.json());
app.use("/api/user", user);
app.use("/api/lists", books);
app.use("/api/reviews", reviews);
app.use("/api/readinglist", readinglist);
app.use("/api/favorite", favorite);
app.listen(3000, function () {
    console.log("Listening on port 3000");
});