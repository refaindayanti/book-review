const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require('config');

const pool = mysql.createPool({
	connectionLimit: 10,
	host: "localhost",
	database: "review_book",
	user: "root",
	password: ""
});

router.post("/register", async function (req, res) {
	let salt = await bcrypt.genSalt(10);
	let hashed = await bcrypt.hash(req.body.password, salt);
	pool.query(`insert into user values ('${req.body.username}', '${hashed}', '0')`, function (err, result) {
		if (err) console.log(err);
		res.status(201).send({ "Username": req.body.username });
	});
});
router.post("/login", function (req, res) {
	pool.query(`select * from user where username = '${req.body.username}'`, async function (err, result) {
		if (err) console.log(err);

		if (result.length <= 0)
			res.status(400).send("Invalid Username");
		else {
			let data_db = result[0];
			if (await bcrypt.compare(req.body.password, data_db.password)) {
				const token = jwt.sign({
					"username": data_db.username,
					"level": data_db.level
				}, "privateKey");
				// }, config.get('jwtPrivateKey'));
				//res.status(200).send({"Username":req.body.username, "Level":result[0].level});
				res.status(200).send(token);
			}
			else {
				res.status(400).send("Invalid Username or Password");
			}
		}
	});
});

module.exports = router;