const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const auth = require("../middleware/auth");
const request = require("request");

const pool = mysql.createPool({
    // connectionLimit: 10,
    host: "localhost",
    database: "review_book",
    user: "root",
    password: ""
});

router.get("/", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        console.log(req.query);
        if (err) throw err;
        var q = '';
        if (req.user.level > 0) {
            if (req.query.book_name) {
                q = `select id_list,book_name,user, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at from reading_list where book_name like '%${req.query.book_name}%' order by created_at desc`;
            } else {
                q = `select id_list,book_name,user, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at from reading_list order by created_at desc`;
            }
        } else {
            if (req.query.book_name) {
                q = `select id_list,book_name,user, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at from reading_list where book_name like '%${req.query.book_name}%' and user='${req.user.username}' order by created_at desc`;
            } else {
                q = `select id_list,book_name,user, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at from reading_list where user='${req.user.username}' order by created_at desc`;
            }
        }
        conn.query(q, function (err, result) {
            conn.release();
            if (err) throw err;
            res.send(result);
        });
    });
});

router.get("/:id", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        var q = '';
        if (req.user.level > 0) {
            q = `select id_list,book_name,user, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at from reading_list where id_list='${req.params.id}' order by created_at desc`;
        } else {
            q = `select id_list,book_name,user, DATE_FORMAT(created_at, '%Y-%m-%d') as created_at from reading_list where id_list='${req.params.id}' and user='${req.user.username}' order by created_at desc`;
        }
        conn.query(q, function (err, result) {
            conn.release();
            if (err) throw err;
            if (result.length > 0) {
                res.send(result[0]);
            } else {
                res.status(404).send('data tidak ditemukan');
            }
        });
    });
});

router.delete("/:id", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        const queryku = `select * from reading_list where user='${req.user.username}' and id_list='${req.params.id}'`;
        conn.query(queryku, function (err, result) {
            console.log('result', result);
            if (result.length > 0) {
                const query = `delete from reading_list where id_list='${req.params.id}'`;
                conn.query(query, function (err, resultDel) {
                    conn.release();
                    if (err) throw err;
                    res.send(result[0]);
                })
            } else res.status(403).send("anda tidak memiliki akses menghapus data ini")
        })
    })
})

router.post("/", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        let q = `insert into reading_list (book_name, user, created_at) values ('${req.body.book_name}', '${req.user.username}','${dateTime}')`;
        conn.query(q, function (err, result) {
            conn.release();
            if (err) throw err;
            res.send(req.body);
        });
    });
});

module.exports = router;