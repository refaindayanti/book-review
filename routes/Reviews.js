const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const auth = require("../middleware/auth");
const request = require("request");

const pool = mysql.createPool({
    // connectionLimit: 10,
    host: "localhost",
    database: "review_book",
    user: "root",
    password: ""
});

router.get("/", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        var q = '';
        if (req.query.book_name) {
            q = `select id_review,book_name,review,user, DATE_FORMAT(created_at, '%Y-%m-%d') as date from review where book_name like '%${req.query.book_name}%' order by created_at desc`;
        } else {
            q = "select id_review,book_name,review,user, DATE_FORMAT(created_at, '%Y-%m-%d') as date from review order by created_at desc";
        }
        conn.query(q, function (err, result) {
            conn.release();
            if (err) throw err;
            res.send(result);
        });
    });
});

router.get("/:id", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        const q = `select id_review,book_name,review,user, DATE_FORMAT(created_at, '%Y-%m-%d') as date from review where id_review='${req.params.id}' order by created_at desc`;
        conn.query(q, function (err, result) {
            conn.release();
            if (err) throw err;
            if (result.length > 0) {
                res.send(result[0]);
            } else {
                res.status(404).send('data tidak ditemukan');
            }
        });
    });
});

router.put("/:id", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        const queryku = `select * from review where user='${req.user.username}' and id_review='${req.params.id}'`;
        conn.query(queryku, function (err, result) {
            if (result.length > 0) {
                const q = `update review set review='${req.body.review}' where id_review='${req.params.id}'`;
                conn.query(q, function (err, resultEdit) {
                    conn.release();
                    if (err) throw err;
                    conn.query(queryku, function (err, resultGet) {
                        res.send(resultGet);
                    })
                });
            } else res.status(403).send("anda tidak memiliki akses mengedit data ini")
        })
    });
})

router.delete("/:id", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        const queryku = '';
        if (req.user.level > 0) {
            queryku = `select * from review where id_review='${req.params.id}'`;
        } else {
            queryku = `select * from review where user='${req.user.username}' and id_review='${req.params.id}'`;
        }
        conn.query(queryku, function (err, result) {
            if (result.length > 0) {
                const query = `delete from review where id_review='${req.params.id}'`;
                conn.query(query, function (err, result) {
                    conn.release();
                    if (err) throw err;
                    res.send(result);
                })
            } else res.status(403).send("anda tidak memiliki akses menghapus data ini")
        })
    })
})

router.post("/", auth, function (req, res) {
    pool.getConnection(function (err, conn) {
        if (err) throw err;
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        let q = `insert into review (book_name, review, user, created_at) values ('${req.body.book_name}', '${req.body.review}', '${req.user.username}','${dateTime}')`;
        conn.query(q, function (err, result) {
            conn.release();
            if (err) throw err;
            res.send(req.body);
        });
    });
});

module.exports = router;