const express = require("express");
const router = express.Router();
const mysql = require("mysql");
const auth = require("../middleware/auth");
const request = require("request");

const pool = mysql.createPool({
    // connectionLimit: 10,
    host: "localhost",
    database: "review_book",
    user: "root",
    password: ""
});

router.get("/", auth, function (req, res) {
    const url = "https://api.nytimes.com/svc/books/v3/lists.json?list=hardcover-fiction&api-key=WGK4tkS4LtKDka5lAApb2ksQXC0GQGQE";
    request.get({ url: url, "rejectUnauthorized": false }, function (error, response, body) {
        if (error) {
            console.log(error);
            res.status(500).send("gagal akses buku")
        } else {
            const hasil = JSON.parse(body);
            res.status(200).send(hasil);
        }
    })
});

module.exports = router;