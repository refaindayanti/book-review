const jwt = require("jsonwebtoken");
const config = require("config")
function auth(req, res, next) {
	const token = req.header("x-auth-token");
	if (!token) {
		return res.status(401).send("No token found");
	}
	let user = {};
	try {
		// user = jwt.verify(token, config.get("jwtPrivateKey"));
		user = jwt.verify(token, "privateKey");
	} catch (error) {
		return res.status(400).send('Invalid token');
	}
	console.log(user);
	const time = new Date(1970, 0, 1);
	time.setSeconds(user.iat);
	console.log(time);
	// cek expired dalam 3 hari 86400 * 3
	// cek expired dalam 5 menit 300 * 3
	if ((new Date().getTime() / 1000) - user.iat > 300 * 3) {
		// (new Date().getTime()/1000) waktu sekarang
		// user.iat -> detik waktu token diciptakan
		return res.status(401).send("Token Expired");
	}

	// if (user.level < 1) // kalau level dibawah 1
	// {
	// 	return res.status(403).send("You are not allowed this resource");
	// }
	req.user = user;
	next();
}

module.exports = auth;